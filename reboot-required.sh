#!/bin/bash

if [ -f /var/run/reboot-required ]; then
  echo 'Reboot is required!'
else
  echo 'All good, no reboot needed!'
fi

